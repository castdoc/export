<!DOCTYPE html>
<html>
    <head>
        <title>JEE - Analysis of pure Java applications</title>
        <link rel="stylesheet" href="styles/site.css" type="text/css" />
        <META http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>

    <body class="theme-default aui-theme-default">
        <div id="page">
            <div id="main" class="aui-page-panel">
                <div id="main-header"><div style="display: inline; white-space: nowrap; margin: 0px 0px 0px 0px;"><input type="button" value="Go to index" onclick="window.open('https://doc.castsoftware.com/export/','_self');" name="HelpTopics"></div>
                    <div id="breadcrumb-section">
                        <ol id="breadcrumbs">
                            <li class="first">
                                <span><a href="index.html">CAST AIP Technologies</a></span>
                            </li>
                                                    <li>
                                <span><a href="CAST+AIP+Technologies+Documentation.html">CAST AIP Technologies Documentation</a></span>
                            </li>
                                                    <li>
                                <span><a href="JEE.html">JEE</a></span>
                            </li>
                                                    <li>
                                <span><a href="JEE+-+Analysis+configuration+and+execution.html">JEE - Analysis configuration and execution</a></span>
                            </li>
                                                    <li>
                                <span><a href="JEE+-+Analysis+configuration.html">JEE - Analysis configuration</a></span>
                            </li>
                                                </ol>
                    </div>
                    <h1 id="title-heading" class="pagetitle">
                                                <span id="title-text">
                            JEE - Analysis of pure Java applications
                        </span>
                    </h1>
                </div>

                <div id="content" class="view">
                    <div class="page-metadata">
                        
        
    
    
        
    
        
        
            Created by <span class='author'> James Hurrell</span>, last modified on Jan 20, 2022
                        </div>
                    <div id="obsolete"><div><strong>This documentation is not maintained. Please refer to <a href="https://doc.castsoftware.com/technologies" target="_blank" rel="noopener noreferrer">doc.castsoftware.com/technologies</a> to find the latest updates.</strong></div></div><div id="main-content" class="wiki-content group">
                    <div class="panel" style="border-width: 1px;"><div class="panelContent">
<p><style type='text/css'>/*<![CDATA[*/
div.rbtoc1706780647575 {padding: 0px;}
div.rbtoc1706780647575 ul {margin-left: 0px;}
div.rbtoc1706780647575 li {margin-left: 0px;padding-left: 0px;}

/*]]>*/</style><div class='toc-macro rbtoc1706780647575'>
<ul class='toc-indentation'>
<li><a href='#JEEAnalysisofpureJavaapplications-Analysisdefinition'>Analysis definition</a></li>
<li><a href='#JEEAnalysisofpureJavaapplications-Supportedfiletypes'>Supported file types</a></li>
<li><a href='#JEEAnalysisofpureJavaapplications-Objectsharing'>Object sharing</a></li>
<li><a href='#JEEAnalysisofpureJavaapplications-RunTimePrediction(InferenceEngine)'>Run Time Prediction (Inference Engine)</a>
<ul class='toc-indentation'>
<li><a href='#JEEAnalysisofpureJavaapplications-Virtualcallresolution'>Virtual call resolution</a></li>
<li><a href='#JEEAnalysisofpureJavaapplications-Stringsevaluation'>Strings evaluation</a></li>
</ul>
</li>
<li><a href='#JEEAnalysisofpureJavaapplications-AppletandSQJsupport'>Applet and SQJ support</a>
<ul class='toc-indentation'>
<li><a href='#JEEAnalysisofpureJavaapplications-Applet'>Applet</a></li>
<li><a href='#JEEAnalysisofpureJavaapplications-SQLJSupport'>SQLJ Support</a>
<ul class='toc-indentation'>
<li><a href='#JEEAnalysisofpureJavaapplications-SQLJDeclarations'>SQLJ Declarations</a></li>
<li><a href='#JEEAnalysisofpureJavaapplications-SQLJStatements'>SQLJ Statements</a></li>
</ul>
</li>
</ul>
</li>
</ul>
</div></p>
</div></div><div class="confluence-information-macro confluence-information-macro-information"><span class="aui-icon aui-icon-small aui-iconfont-info confluence-information-macro-icon"></span><div class="confluence-information-macro-body"><strong>Summary</strong>: this page provides information about how the JEE Analyzer handles pure Java applications</div></div><h1 id="JEEAnalysisofpureJavaapplications-Analysisdefinition">Analysis definition</h1><p>In the context of a pure Java analysis an analysis is defined in terms of:</p><ul><li>a set of primary files</li><li>a set of classpath entries</li></ul><p>CAST fully analyses all primary files and uses classpath entries to resolve external references. External files are only analyzed on demand and only objects that are referenced in primary files are retrieved. In others words, primary objects (with all their incoming and outgoing references) are always available independently of their usage, whilst external objects are stored only when used and with only incoming but no outgoing references. For instance if an external class contains several methods and only one is used within the application, the remaining methods will not be stored. External objects can be identified in the viewer by their grey shading.</p><p>An external class can only be found under a classpath entry if the commonly adopted file deployment convention is respected where package hierarchy maps to directory structure and where file names identify classes. This restriction obliviously does not apply to primary files.</p><p>Very large applications (tens of thousands of files) that cannot be handled in a single analysis can be split into several smaller ones and analyzed independently of each other. This break down can almost be done arbitrarily and will not lead to any missing or incorrect information (see more in the <strong> Object Sharing</strong> section below). When specifying each &quot;sub-analysis&quot;, simply ensure that the whole application is accessible via classpath.</p><p>CAST allows each analysis to use their own &quot;core API libraries&quot; and cannot run if system classes (i.e. &quot;java lang.*&quot;) are not specified in the Classpaths field - see <strong><a href="JEE+-+Analysis+configuration.html">JEE - Analysis configuration</a></strong>.</p><h1 id="JEEAnalysisofpureJavaapplications-Supportedfiletypes">Supported file types</h1><p>The JEE Analyzer supports java source files (.java), class files (.class) and SQLJ files (.sqlj).</p><p>Class files are reversed (or &quot;decompiled&quot;) before being processed by the analyzer. By design, only stub files are then regenerated to extract object hierarchy. However, method implementations and variable initializers are not reversed.</p><p>Java archives (.jar and .zip) cannot be specified as such as analysis files. To include zipped files, the archive that contains them must be unzipped first. On the other hand archives are supported as class path entries. However, for archive processing, software requires the &quot;java&quot; executable program (delivered with any SDK or JDK) to be installed and accessible via the &quot;Path&quot; system variable on the computer where the analysis is run. Note that if the actual implementation source code for a given itemis provided in a Java archive, then the  analyzer will handle it as external code and will only be able to decompile the objects as signatures.</p><h1 id="JEEAnalysisofpureJavaapplications-Objectsharing">Object sharing</h1><p>All objects revealed by the analyzer are not only identified by their Java qualified name (including mangling to distinguish overloaded methods) but also by the full path of their corresponding definition file. This allows the reflection of dynamic class loading paradigm and distinguishes between several implementations of the same class.</p><p>However objects are not identified by the analysis in which they are retrieved. Hence if a file is processed in the context of distinct analyses the same set of objects will be shared between those analyses. This allows the creation of consistent results when processing an application in several analyses.</p><p>Each analysis keeps for each file an image corresponding to a snapshot at the time when file was last processed within the current analysis. Consequently if a shared file is modified it needs to be analyzed in the context of each and every analysis that includes the shared file. All non-resynchronized analyses will otherwise reflect the file as it was before modification.</p><h1 id="JEEAnalysisofpureJavaapplications-RunTimePrediction(InferenceEngine)">Run Time Prediction (Inference Engine)</h1><p>The J2EE Analyzer uses an inference engine to compute run time type information in order to simulate program behaviour during its execution. To find out more about activating and deactivating it in the CAST Management Studio, see <strong><a href="JEE+-+Analysis+configuration.html">JEE - Analysis configuration</a></strong>. This information is used to address the two following issues:</p><ul><li>Resolution of virtual calls</li><li>Evaluation of character strings</li></ul><h2 id="JEEAnalysisofpureJavaapplications-Virtualcallresolution">Virtual call resolution</h2><p>Method calls are de-virtualized by computing the actual run time type of the object on which invocation is performed. Software navigates from instantiation sites to method calls using executable paths. In case of alternatives both branches are considered so that all run time possibilities are retained.</p><p>Example:</p><div class="code panel pdl" style="border-width: 1px;"><div class="codeContent panelContent pdl">
<pre class="syntaxhighlighter-pre" data-syntaxhighlighter-params="brush: java; gutter: false; theme: DJango" data-theme="DJango">interface I { void foo(); }
class A implements I { void foo() {...} }
class B implements I { void foo() {...} }
class T {
        void doSmth(int i) {
                I anObject = null;
                if (i &gt; 0)
                        anObject = new A();
                else
                        anObject = new B();
                anObject.foo();
        }
</pre>
</div></div><p>On anObject.foo() software will trace a call towards I.foo() by compile time type analysis but will also trace dynamic calls towards A.foo() and B.foo() using run time type information.</p><h2 id="JEEAnalysisofpureJavaapplications-Stringsevaluation">Strings evaluation</h2><p>String evaluation is used within the <strong>Parametrization</strong> feature (see the <strong><a href="JEE+-+Environment+Profiles.html">JEE - Environment Profiles</a></strong>) to compute the actual value of arguments of parametrized methods. Here also all possibilities are evaluated and all possible links are traced accordingly.</p><p>Example:</p><p>Lets suppose that T.execSQL(java.lang.String) is parametrized to recognize its parameter as a server object.</p><div class="code panel pdl" style="border-width: 1px;"><div class="codeContent panelContent pdl">
<pre class="syntaxhighlighter-pre" data-syntaxhighlighter-params="brush: java; gutter: false; theme: DJango" data-theme="DJango">    --&gt; class T {
         void execSQL(String s) { ... }
         void doSelect(String s) {
                 String req = &quot;select * from My&quot;;
                 execSQL(req + s)
         }
         void doSmth() {
                 doSelect(&quot;Table&quot;);
         }
 }
</pre>
</div></div><p>On doSelect(&quot;Table&quot;) software will trace a &quot;Use Select&quot; link towards table MyTable if any.</p><h1 id="JEEAnalysisofpureJavaapplications-AppletandSQJsupport">Applet and SQJ support</h1><h2 id="JEEAnalysisofpureJavaapplications-Applet">Applet</h2><p>Whenever an applet is detected (either via an HTML &lt;APPLET&gt; tag or via a class extending java.applet.Applet) an applet component is created in the Analysis Service. Applets are identified by the full path of their corresponding class file. The <strong>Prototype</strong> link is traced between the applet and its applet class.</p><h2 id="JEEAnalysisofpureJavaapplications-SQLJSupport">SQLJ Support</h2><p>The JEE Analyzer supports the <strong>SQLJ extension</strong>. It does not preprocess code to its JDBC equivalent as the SQLJ translator does, instead, it enriches underlying roadmaps with objects implicitly defined by SQLJ declarations.</p><p>The following SQLJ language elements are supported: </p><ul><li>SQLJ declaration: iterator (named and positional) and context declaration both with &quot;implements&quot; and &quot;with&quot; clauses</li><li>SQLJ executable statement: statement or assignment clause</li></ul><h3 id="JEEAnalysisofpureJavaapplications-SQLJDeclarations">SQLJ Declarations</h3><p><strong>Implicit Inheritance</strong> is recreated as follows on each declaration:</p><div class="table-wrap"><table class="wrapped confluenceTable"><colgroup><col/><col/><col/></colgroup><tbody><tr><th class="confluenceTh"><strong> </strong></th><th class="confluenceTh">Implicit super-class</th><th class="confluenceTh">Implicit super-interface</th></tr><tr><th class="confluenceTh">Positional iterator</th><td class="confluenceTd">sqlj.runtime.ref.ResultSetIterImpl</td><td class="confluenceTd">sqlj.runtime.PositionedIterator</td></tr><tr><th class="confluenceTh">Named iterator</th><td class="confluenceTd">sqlj.runtime.ref.ResultSetIterImpl</td><td class="confluenceTd">sqlj.runtime.NamedIterator</td></tr><tr><th class="confluenceTh">Context</th><td class="confluenceTd">sqlj.runtime.ref.ConnectionContextImpl</td><td class="confluenceTd">sqlj.runtime.ConnectionContext</td></tr></tbody></table></div><p>For all declarations that use the &quot;with&quot; clause, a class attribute is created on each &quot;with&quot; constant. The type of this attribute is given by the resolved type of its initializer.</p><p>The following <strong>implicit constructors</strong> are defined for an<strong> iterator declaration</strong>:</p><ul><li>public &lt;<em>iterator name</em>&gt; ( sqlj.runtime.profile.RTResultSet )</li></ul><p>For each column of an <strong>iterator declaration</strong> an <strong>implicit accessor</strong> is defined as follows:</p><ul><li>named iterator: public &lt;<em>column type</em>&gt; get&lt;<em>column name</em>&gt; ( )</li><li>unnamed iterator: public &lt;<em>column type</em>&gt; getCol&lt;<em>column index</em>&gt; ( )</li></ul><p>The following <strong>implicit methods</strong> are defined for a <strong>context declaration</strong>:</p><ul><li>public static &lt;<em>context name</em>&gt; getDefaultContext ( )</li><li>public static void setDefaultContext (&lt;<em>context name</em>&gt; )</li><li>public static sqlj.runtime.profile.Profile getProfile (java.lang.Object )</li><li>public static java.lang.Object getProfileKey (sqlj.runtime.profile.Loader , java.lang.String )</li></ul><p>The following <strong>implicit constructors</strong> are defined for a <strong>context declaration</strong>:</p><ul><li>public &lt;<em>context name</em>&gt; ( java.sql.Connection )</li><li>public &lt;<em>context name</em>&gt; ( sqlj.runtime.ConnectionContext )</li><li>public &lt;<em>context name</em>&gt; ( java.lang.String , boolean )</li><li>public &lt;<em>context name</em>&gt; ( java.lang.String , java.util.Properties , boolean ) </li><li>public &lt;<em>context name</em>&gt; ( java.lang.String , java.lang.String , java.lang.String , boolean )</li></ul><h3 id="JEEAnalysisofpureJavaapplications-SQLJStatements">SQLJ Statements</h3><p>While processing <strong>SQLJ clauses</strong>, Java <strong>host expressions</strong>, <strong>context expressions</strong> and <strong>result expressions</strong> are passed to the analyzer as normal Java code. At the same time, the embedded SQL code is scanned for server side object references using dynamic link resolution algorithms as explained in the corresponding section.</p>
                    </div>

                 
                </div>             </div> 
            <div id="footer" role="contentinfo">
                <section class="footer-body">
                    <p>Document generated by Confluence on Feb 01, 2024 10:44</p>
                    <div><br /><a target="_blank" href="http://www.castsoftware.com"><img border="0" src="images/logo.jpg" ></a></div>
                </section>
            </div>
        </div>     </body>
</html>
